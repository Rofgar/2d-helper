import org.openrndr.MouseButton
import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.LineCap
import org.openrndr.extra.olive.oliveProgram
import org.openrndr.math.Vector2
import org.openrndr.math.Vector3

fun Vector2.insideCircle(circlePoint: Vector2, circleRadius: Double): Boolean {
    return distanceTo(circlePoint) <= circleRadius
}

fun Vector2.newCircleInsideCircle(circlePoint: Vector2, circleRadius: Double): Boolean {
    return distanceTo(circlePoint) - circleRadius / 2 <= circleRadius
}

fun main() = application {
    configure {
        width = 760
        height = 560
        title = "2D Helper"
    }

    oliveProgram {
        val circles = mutableListOf<Vector2>()
        val circleRadius = 9.0
        val circleStrokeWeight = 0.6
        val circleColor = ColorRGBa.RED

        val vectors = mutableListOf<Pair<Vector2, Vector2>>()

        var mouseOriginUp: Vector2? = null
        var mouseDragPosition: Vector2? = null

        mouse.buttonUp.listen { mouse ->
            if (mouse.button == MouseButton.LEFT) {

                val originCircle = circles.firstOrNull { mouseOriginUp?.insideCircle(it, circleRadius) ?: false }
                val lastDragCircle = circles.firstOrNull { mouse.position.insideCircle(it, circleRadius) }

                if (originCircle != null && lastDragCircle != null) {
                    vectors.add(originCircle to lastDragCircle)
                } else {
                    if (!circles.any { mouse.position.newCircleInsideCircle(it, circleRadius) }) {
                        circles.add(mouse.position)
                    }
                    if (mouseOriginUp != null) {
                        if (!circles.any { mouseOriginUp!!.newCircleInsideCircle(it, circleRadius) }) {
                            circles.add(mouseOriginUp!!)

                            vectors.add(mouse.position to mouseOriginUp!!)
                        }
                    }
                }

                mouseOriginUp = null
                mouseDragPosition = null
            } else if (mouse.button == MouseButton.RIGHT) {
                circles.indexOfFirst { mouse.position.insideCircle(it, circleRadius) }.let {
                    if (it > -1) {
                        circles.removeAt(it)
                    }
                }
            }
        }

        mouse.buttonDown.listen { mouse ->
            if (mouse.button == MouseButton.LEFT) {
                mouseDragPosition = null
                mouseOriginUp = mouse.position
            }
        }

        mouse.dragged.listen { mouse ->
            if (mouse.button == MouseButton.LEFT) {
                mouseDragPosition = mouse.position
            }
        }

        extend {
            drawer.clear(ColorRGBa.WHITE)

            circles.forEach { circle ->
                drawer.fill = circleColor
                drawer.strokeWeight = circleStrokeWeight
                drawer.circle(circle, circleRadius)

                drawer.fill = ColorRGBa.BLACK
                val text = "(${circle.x.toInt()}, ${circle.y.toInt()})"
                drawer.text(text, circle.x - 5, circle.y + circleRadius * 2.5)
            }

            if (mouseDragPosition != null && mouseOriginUp != null) {
                drawer.stroke = ColorRGBa.BLACK
                drawer.strokeWeight = 1.5
                drawer.lineCap = LineCap.ROUND
                drawer.lineSegment(mouseOriginUp!!, mouseDragPosition!!)
            }

            vectors.forEach {
                drawer.stroke = ColorRGBa.BLACK
                drawer.strokeWeight = 1.5
                drawer.lineCap = LineCap.ROUND
                drawer.lineSegment(it.first, it.second)

                val forward = it.first.minus(it.second)
                val up = Vector3(0.0, 0.0, 1.0);
                val right = forward.normalized.vector3().cross(up.normalized).xy * it.first.distanceTo(it.second)

                drawer.lineSegment(it.first, it.first + right)
            }
        }
    }
}
